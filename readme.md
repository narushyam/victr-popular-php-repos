# Victr - Get popular PHP repositories on GitHub

# Prerequisites

* All Laravel Dependencies
 * PHP >= 5.5.9
 * OpenSSL PHP Extension
 * PDO PHP Extension
 * Mbstring PHP Extension
 * Tokenizer PHP Extension
* Composer
* Gulp
* npm
* bower

# Installation

1. Clone the repository.
2. `composer install`
3. `npm install`
4. `bower install`
5. `./node_modules/bower-installer/bower-installer.js`
6. `gulp`

# Data base connections

* In .env file specify your database connections such as database username, password and name of the database.
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
* run `php artisan migrate` this will create tables in database.

# Creating virtual host

* By default laravel project points to public directory.
* You should map your virtual host to 'victr-popular-php-repos/public' directory.

# Using localhost

* If you are using localhost just type 'localhost/victr-popular-php-repos/public' in your browser.

## Additional info

* I used Homestead as my development environment, I recommend you to use homestead where all the Laravel Dependencies are intalled by default, however you are free to use any other hosting solution as well.

* Click the 'Repositories' menu to see list of all popular php repositories.

* Click 'View' to view detail of the repository.

* Click 'Update Repositories' to check for updates and get updated list.