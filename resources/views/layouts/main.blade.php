<!DOCTYPE html>
<html ng-app="app">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ env("APP_NAME") }}</title>

    @include('shared.assets_css')

</head>

<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}">Home</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="{{ (Request::path() == 'repositories') ? 'active' : '' }}"><a href="{{action('RepositoryController@index')}}">Repositories <span class="sr-only">(current)</span></a></li>
        </li>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">

        <div class="wrapper wrapper-content">
            @yield('content')
        </div>
    </div>
</div>


@include('shared.assets_js')

</body>
</html>