@extends('layouts.main')

@section('content')
<div class="container" align="center">
    <div class="row">
        <div class="col-md-12"><h1 class="section-header">Repository Details</h1></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Rrepository Id:</label> {{$repository->repository_id}}
            </div>      
            <div class="form-group">
                <label>Name:</label> {{$repository->name}}
            </div>
            <div class="form-group">
                <label>Description:</label> {{$repository->description}}
            </div>
            <div class="form-group">
                <label>Url:</label> {{$repository->url}}
            </div>
            <div class="form-group">
                <label>Created:</label> {{$repository->created_date}}
            </div>
            <div class="form-group">
                <label>Last Push:</label> {{$repository->last_push_date}}
            </div>
            <div class="form-group">
                <label>Stars:</label> {{$repository->no_stars}}
            </div>
        </div>
        <div class="col-md-12">
            <a href="{{ url('repositories')}}" class="btn btn-primary">Back</a>
        </div>
    </div>
</div>
@endsection