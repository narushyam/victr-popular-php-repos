@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10"><h1 class="section-header">Popular PHP Repositories on GitHub</h1></div>
        <div class="col-md-2"><a href="{{ url('repository/update')}}" class="btn btn-info">Update Repositories</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tr>
                    <th>Repository Id</th>
                    <th>Name</th>
                    <th>Url</th>
                    <th>Created</th>
                    <th>Last Push</th>
                    <th>Stars</th>
                    <th>Action</th>
                </tr>
                @foreach($repositories as $repository)
                <tr>
                    <td>{{e($repository->repository_id)}}</td>
                    <td>{{e($repository->name)}}</td>
                    <td>{{e($repository->url)}}</td>
                    <td>{{e($repository->created_date)}}</td>
                    <td>{{e($repository->last_push_date)}}</td>
                    <td>{{e($repository->no_stars)}}</td>
                    <td><a href="{{ url('repository', $repository->id)}}" class="btn btn-info">View</a></td>
                <tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="row" align="center">
        <div class="col-md-12">
            {{$repositories->links()}}
        </div>
    </div>
</div>
@endsection