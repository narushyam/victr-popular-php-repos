<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRepositories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repositories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('repository_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->text('url');
            $table->dateTime('created_date');
            $table->dateTime('last_push_date');
            $table->integer('no_stars');
            $table->timestamps();
            $table->index('repository_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('repositories');
    }
}
