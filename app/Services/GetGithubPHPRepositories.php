<?php
namespace App\Services;

class GetGithubPHPRepositories
{
    /**
     * Get popular php repositories from github api.
     *
     * @return array
     */
    public function get()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.github.com/search/repositories?q=language:php&sort=stars&order=desc&per_page=100',
            CURLOPT_USERAGENT => 'Victr'
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        $repositories = json_decode($resp);
        $array_data = [];
        foreach ($repositories->items as $repo) {
            $data = ['repository_id'=>$repo->id, 'name'=>$repo->name, 'description'=>$repo->description, 'url'=>$repo->html_url, 'created_date'=>$repo->created_at, 'last_push_date'=>$repo->pushed_at, 'no_stars'=>$repo->stargazers_count, 'created_at' => date('y-m-d H:i:s'), 'updated_at' => date('y-m-d H:i:s')];
            array_push($array_data, $data);
        }
        return $array_data;        
    }
}