<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repository;

use App\Services\GetGithubPHPRepositories;

class RepositoryController extends Controller
{
    private $getGithubPHPRepositories;

    public function __construct(GetGithubPHPRepositories $getGithubPHPRepositories)
    {
        $this->getGithubPHPRepositories = $getGithubPHPRepositories;
    }

    /**
     * Display all repositories.
     *
     * @return Response
     */
    public function index()
    {
        //Create repositories in database if not exists;
        if (Repository::count() == 0) {
            $repositories = $this->getGithubPHPRepositories->get();
            Repository::insert($repositories);
        }
        return View('repositories.index')->with('repositories', Repository::orderBy('no_stars', 'desc')->paginate(20));
    }

    /**
     * Display the specified respository.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return View('repositories.show')->with('repository', Repository::find($id));
    }


    /**
     * Update repositories in database and create if not exists.
     *
     * @return Response
     */
    public function update()
    {
        $repositories = $this->getGithubPHPRepositories->get();
        foreach ($repositories as $repo) {
            $repository = Repository::where('repository_id', $repo['repository_id'])->first();
            if (!count($repository)) {
                $repository = new Repository; 
            }
            $repository->repository_id = $repo['repository_id'];
            $repository->name = $repo['name'];
            $repository->description = $repo['description'];
            $repository->url = $repo['url'];
            $repository->created_date = $repo['created_date'];
            $repository->last_push_date = $repo['last_push_date'];
            $repository->no_stars = $repo['no_stars'];
            $repository->save();
        }
        return redirect()->action('RepositoryController@index');

    }
}
