<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repository extends Model
{
    protected $table = 'repositories';

    protected $fillable = ['repository_id', 'name', 'description', 'url', 'created_date', 'last_push_date', 'no_stars'];
}
